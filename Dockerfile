FROM quay.io/keboola/docker-custom-python:latest

COPY . /code/
COPY /data/ /data/
RUN pip install flake8

RUN pip install --ignore-installed -r /code/requirements.txt

WORKDIR /code/

CMD ["python3", "-u", "/code/src/component.py"]
