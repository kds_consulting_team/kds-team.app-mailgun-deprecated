import datetime
import re
import logging


def delivery_time_check(delivery_time):
    """
    Dummy function that checks, whether delivery time was correctly inputted.
    """
    pattern = r'((20)[0-9]{2})-([0][1-9]|[1][0-2])-([0][1-9]|[1-2][0-9]|[3][0-1])' + \
        r'\s{1}([0|1][0-9]|[2][0-3]):[0-5][0-9]:[0-5][0-9]\s{1}(\+|-)([0|1][0-9]{3})'

    if re.fullmatch(pattern, str(delivery_time)):
        scheduled_delivery = datetime.datetime.strptime(delivery_time,
                                                        '%Y-%m-%d %H:%M:%S %z')\
            .strftime('%a, %d %b %Y %H:%M:%S %z')
        logging.info("Delivery scheduled for %s." % scheduled_delivery)
    elif delivery_time == '':
        scheduled_delivery = None
        logging.info(
            "Delivery time was not inputted. Message will be delivered ASAP.")
    else:
        scheduled_delivery = None
        msg1 = "Delivery time was inputted wrong. %s is unsupported." % delivery_time
        msg2 = "Message will be delivered straightaway."
        logging.warn(" ".join([msg1, msg2]))

    return scheduled_delivery
