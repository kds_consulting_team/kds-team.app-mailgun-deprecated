**0.6.4**
ECR build and uplink.

**0.6.3**
Changed type of error, that is raised when a parameter is missing. Prompts user to enter the missing parameter.

**0.6.1**
Logs are incrementally loaded to storage.

**0.6.0**
Changed the way of scheduled delivery, for more precise results. The scheduled delivery time should now be inputted in format `YYYY-MM-DD HH:MM:SS ±ZZZZ`.

**0.5.4**
Fixed bug, that caused application crash, when fetching logs for mailing list of length 0.

**0.5.3**
Updated README
Added LICENSE

**0.5.2**
Added GELF logging

**0.5.1**
Fixed unittests

**0.5.0**
Added logs fetcher which will, by default, save logs to *out.c-mailgun.logs*